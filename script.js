document.getElementById("countButton").onclick = function() {
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    const letterArray = {};
    let wordArray = {};
    wordArray = typedText.split(" ")
    for (let i = 0; i < typedText.length; i++) {
        let currentLetter = typedText[i];
        if (letterArray[currentLetter] === ""){continue} 
        else if(letterArray[currentLetter] === undefined) {letterArray[currentLetter] = 1} 
        else {letterArray[currentLetter]++}
    }
    for (let letter in letterArray) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterArray[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }
    for (let i = 0; i < wordArray.length; i++) {
        let currentWord = wordArray[i];
        if(wordArray[currentWord] === undefined) {wordArray[currentWord] = 1}
        else {wordArray[currentWord]++}
    }
    for (let word in wordArray) {
        if (isNaN(word)){
            const span = document.createElement("span");
            const textContent = document.createTextNode('"' + word + "\": " + wordArray[word] + ", ");
            span.appendChild(textContent);
            document.getElementById("wordsDiv").appendChild(span);
        }
    }
}
